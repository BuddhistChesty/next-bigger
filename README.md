## Task definition ##

Implement the [NextBiggerThan](NextBigger/NumberExtension.cs#L15) method which finds the nearest largest integer consisting of the digits of the given positive integer number and null if no such number exists.

*Topics - loops, algorithms*