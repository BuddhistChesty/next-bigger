﻿using System;

namespace NextBiggerTask
{
    public static class NumberExtension
    {
        public static int? NextBiggerThan(int number)
        {
            int count1 = 0;
            int count = 0;
            int temp = number;

            if (number < 0)
            {
                throw new ArgumentException($"Value of {nameof(number)} cannot be less zero.", nameof(number));
            }

            if (number == int.MaxValue)
            {
                return null;
            }

            while (temp != 0)
            {
                count++;
                temp /= 10;
            }

            static void Swap(int[] number, int i, int j)
            {
                int temp = number[i];
                number[i] = number[j];
                number[j] = temp;
            }

            void SortSubarray(int[] number, int i, int j)
            {
                while (i < j)
                {
                    Swap(number, i, j);
                    i += 1;
                    j -= 1;
                }
            }

            void FindNextGreaterNumber(int[] number)
            {
                int lastDigitSeen = number[number.Length - 1], i, j;
                for (i = number.Length - 2; i >= 0; i--)
                {
                    if (lastDigitSeen > number[i])
                    {
                        break;
                    }

                    lastDigitSeen = number[i];
                }

                if (i >= 0)
                {
                    for (j = number.Length - 1; j > i; j--)
                    {
                        if (number[j] > number[i])
                        {
                            break;
                        }
                    }

                    Swap(number, i, j);

                    SortSubarray(number, i + 1, number.Length - 1);
                }
            }

            int[] numbers = new int[count];
            temp = number;
            for (int i = 1; i <= count; i++)
            {
                numbers[count - i] = temp % 10;
                temp /= 10;
            }

            count1 = count;
            int first = number % 10;
            for (int i = 2; i <= count; i++)
            {
                if (first == numbers[count - i])
                {
                    count1--;
                }
            }

            if (count1 <= 1)
            {
                return null;
            }

            count1 = count;
            for (int i = 0; i < count; i++)
            {
                if (numbers[i] == 0)
                {
                    count1--;
                }
            }

            if (count1 <= 1 || count == 1)
            {
                return null;
            }

            FindNextGreaterNumber(numbers);
            temp = 0;
            for (int i = 1; i <= count; i++)
            {
                temp += numbers[count - i] * (int)Math.Pow(10, i - 1);
            }

            return temp;
        }
    }
}
